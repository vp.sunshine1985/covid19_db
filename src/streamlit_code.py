import pandas as pd
import streamlit as st
import plotly.graph_objects as go
import plotly.express as px
from plotly.subplots import make_subplots

#!!! change the branch in the URLs
DATA_URL = 'https://gitlab.com/datanomiqopensource/covid19_db/-/raw/master/datasets/covid19_data.csv'
DATA_LATEST_URL = 'https://gitlab.com/datanomiqopensource/covid19_db/-/raw/master/datasets/covid19_data_latest.csv' 

@st.cache
def load_data(url):
    data = pd.read_csv(url)
    data['Date'] = pd.to_datetime(data['Date']).dt.date 
    return data
data = load_data(DATA_URL)
data_latest = load_data(DATA_LATEST_URL).drop(columns = ['Unnamed: 0', 'Unnamed: 0.1'])

# calculate the constants
latest_date =  data['Date'].max()
oldest_date = data['Date'].min()
list_of_countries = list(set(data['Country']))
list_of_countries.sort()
list_of_continents = list(set(data['Continent']))
list_of_continents.sort()

#### Title
from PIL import Image
#image = Image.open('banner.jpg')
image = Image.open('../banner.jpg')
st.image(image, use_column_width=True)

def make_map(data):
    map_plot = go.Figure(data = go.Choropleth(
                                locations=data_latest['ISO'],
                                z = data_latest['ActiveCases'],
                                colorscale = 'Purd',
                                colorbar_title = "Cases"))
    map_plot.update_layout(
        title_text = 'Active Covid-19 cases worldwide.',
        geo = dict(showframe=False,
                   showcoastlines=False,
                   projection_type="orthographic"),
        height = 600,
        paper_bgcolor='rgb(242, 242, 242)')
    return map_plot

map_plot = make_map(data)
map_plot

all_cases_world = int(data_latest['ConfirmedCases'].sum())
sick_world = int(data_latest['ActiveCases'].sum()) 
recovered_world = int(data_latest['RecoveredCases'].sum())
deaths_world = int(data_latest['Deaths'].sum())

sick_china = int(data_latest['ActiveCases'][data_latest['Country'] == 'China']) 
recovered_china = int(data_latest['RecoveredCases'][data_latest['Country'] == 'China'])
deaths_china = int(data_latest['Deaths'][data_latest['Country'] == 'China'])

sick_germany = int(data_latest['ActiveCases'][data_latest['Country'] == 'Germany']) 
recovered_germany = int(data_latest['RecoveredCases'][data_latest['Country'] == 'Germany'])
deaths_germany = int(data_latest['Deaths'][data_latest['Country'] == 'Germany'])

sick_italy = int(data_latest['ActiveCases'][data_latest['Country'] == 'Italy'])  
recovered_italy = int(data_latest['RecoveredCases'][data_latest['Country'] == 'Italy'])
deaths_italy = int(data_latest['Deaths'][data_latest['Country'] == 'Italy'])

@st.cache     
def make_pie():
    pie = make_subplots(2, 2, specs=[[{'type':'domain'}, {'type':'domain'}],
                                     [ {'type':'domain'}, {'type':'domain'}]],
                        subplot_titles=['World', 'China', 'Germany', 'Italy'])
    pie.add_trace(go.Pie(labels = ['Sick', 'Recovered', 'Deaths'], 
                         values = [sick_world,
                                   recovered_world,
                                   deaths_world],
                         scalegroup='one',
                         name="World"),
                         1, 1)
    
    pie.add_trace(go.Pie(labels = ['Sick', 'Recovered', 'Deaths'], 
                         values = [sick_china,
                                   recovered_china,
                                   deaths_china],
                         scalegroup='one',
                         name="China"),
                         1, 2)
    pie.add_trace(go.Pie(labels = ['Sick', 'Recovered', 'Deaths'], 
                         values = [sick_germany,
                                   recovered_germany,
                                   deaths_germany],
                         scalegroup='one',
                         name="Germany"),
                         2, 1)
    pie.add_trace(go.Pie(labels = ['Sick', 'Recovered', 'Deaths'], 
                         values = [sick_italy,
                                   recovered_italy,
                                   deaths_italy],
                         scalegroup='one',
                         name="Italy"),
                         2, 2)
    pie_colors = ['rgb(213, 71, 115)', 'rgb(127, 127, 127)', 'rgb(254, 164, 1)' ]
    pie.update_traces(hole = .4, 
                      hoverinfo='label+percent', 
                      textinfo='value', 
                      textfont_size=15,
                      textposition='outside',
                      marker=dict(colors=pie_colors))
    pie.update_layout(
        title_text='Reported Covid-19 cases. Last update: {}'.format(latest_date),
        font=dict(size=15),
        # Add annotations in the center of the donut pies.
        annotations=[dict(text='World', x=0.225, y=0.48, font_size=15, showarrow=False),
                     dict(text='China', x=0.78, y=0.48, font_size=15, showarrow=False),
                     dict(text='Germany', x=0.225, y=0, font_size=15, showarrow=False),
                     dict(text='Italy', x=0.78, y=0, font_size=15, showarrow=False)],
        paper_bgcolor='rgb(242, 242, 242)',
        plot_bgcolor='rgb(242, 242, 242)',
        height=650)
    return pie

pie_plot = make_pie()
pie_plot


st.header('''All Covid-19 cases include currently sick patients, recovered patients and deaths. Germany is currently on the $${}$$. place by number of all confirmed Covid-19 cases in the world'''
.format(int(data_latest['Rank_all'][data_latest['Country'] == 'Germany']), ))
st.write('Hover over to see the number of cases in top 10 countries by total number of cases')

@st.cache
def make_top_10(rank_col, value_col, title_text):   
    colors = ['rgb(127, 127, 127)']*10
    if 'Germany' in list(data_latest['Country'][data_latest[rank_col] <= 10]):  
        index_germany = list(data_latest['Country'][data_latest[rank_col] <= 10]).index('Germany')
        colors[index_germany] = 'rgb(213, 71, 115)'
        
    bar1 = go.Figure()
    bar1.add_trace(go.Bar(
                            x = list(data_latest[value_col][data_latest[rank_col] <= 10]),
                            y = list(data_latest['Country'][data_latest[rank_col] <= 10]),
                            orientation='h',
                            marker_color=colors,
                            text = list(data_latest[value_col][data_latest[rank_col] <= 10]),
                            textposition = 'inside',
                            ))

    bar1.update_layout(yaxis={'categoryorder':'total ascending'},
                       xaxis = dict(range=[0, data_latest['ConfirmedCases'].max()]),
                       title_text= title_text,
                       font=dict(size=13),
                       paper_bgcolor='whitesmoke',
                       plot_bgcolor='rgba(0,0,0,0)',
                       height = 450)
    return bar1
top_10_all = make_top_10('Rank_all', 'ConfirmedCases', 
                         'Top 10 countries by total number of cases')
top_10_all

top_10_active = make_top_10('Rank_active', 'ActiveCases', 
                            'Top 10 countries by number of active cases')
top_10_active

st.header('To see how all the countries with >1000 cases click on the continent name.')
st.write('Hover over to see the exact number of cases')
@st.cache
def make_subburst(data_latest):
    burst = px.sunburst(data_latest.sort_values(by=['ConfirmedCases' ], 
                    ascending = False).head(20), path = ['Continent', 'Country'], 
                    values = 'ConfirmedCases',
                    color='Continent',
                    color_discrete_map={'(?)':'black',
                                        'Europe':'rgb(116, 26, 54)',
                                        'Asia':'rgb(173, 40, 81)',
                                        'North America':'rgb(180, 24, 110)',
                                        'Oceania' : 'rgb(120, 16, 73)'})
    burst.update_traces(
            go.Sunburst(hovertemplate='<b>%{label} %{value}'))
    burst.update_layout(
            #title_text='Reported Covid-19 cases. Countries with over 1000 cases',
            font=dict(size=15),
            paper_bgcolor='whitesmoke')
    return burst

sunburst_plot = make_subburst(data_latest)
sunburst_plot

#### TODO
# def make_pop_perc(data_latest):
#     pop_perc = make_subplots(rows = 1, cols = 2)
#     pop_perc.add_trace(go.Pie(
#         labels = ['Healthy', 'Sick'], 
#         values = [sick_world,
                  
#                   ],
#         ), row = 1, col = 1)
#     pop_perc.add_trace(go.Pie(
#         ), row = 1, col = 2)
    

#### selectbox country
st.header('Choose a country to see the timeline of Covid_19 development.')

country = st.selectbox('Choose a country: ', 
                       options = list_of_countries, 
                       key = 'Country')

st.header('You can see the cumulative number of cases in the first plot and the newly reported cases in the last 7 days in the second one.')
st.write('Click and drag to zoom in on a timeframe. Hover to see the exact numbers.')

def make_timeline():  
    timeline = go.Figure()
    timeline.add_trace(go.Scatter(
        x = data['Date'][data['Country'] == country],
        y = data['ConfirmedCases'][data['Country'] == country],
        mode = 'lines+markers'))
    timeline.update_layout(
                title_text='Timeline of reported Covid-19 cases in {}'.format(country),
                font=dict(size=15),
                plot_bgcolor='rgba(0,0,0,0)',
                paper_bgcolor='whitesmoke'
                )
    return timeline

timeline = make_timeline()
timeline

#### TODO

# data_new_cases_14 = data.set_index(data['Date'])
# data_new_cases_14 = data_new_cases_14[data_new_cases_14['Date'] >= 
#                                     (pd.to_datetime(latest_date) - pd.Timedelta(13, unit='d')))]

# def make_bar(data):
#     bar = go.Figure(
#         data = [go.Bar(name = 'New Sick', 
#                        x = data['Date'] >= (pd.to_datetime(latest_date) - pd.Timedelta(13, unit='d'))),
#                        y = data['NewSick'][data['Country'] == country],
#                        marker_color = 'rgb(213, 71, 115)'),
#                 go.Bar(name = 'New Recovered', 
#                        x = data['Date'],
#                        y = data['NewRecovered'][data['Country'] == country],
#                        marker_color = 'rgb(127, 127, 127)'),
#                 go.Bar(name = 'New Deaths', 
#                        x = data['Date'],
#                        y = data['NewDeaths'][data['Country'] == country],
#                        marker_color = 'rgb(254, 164, 1)')])
#     bar.update_layout(barmode='stack',
#                       title_text='New cases reported in the last 14 days in {}'.format(country),
#                       font=dict(size=16),
#                       paper_bgcolor='whitesmoke',
#                       plot_bgcolor='rgba(0,0,0,0)'
#                       )
#     return bar
# bar_plot = make_bar(data_new_cases_14)
# bar_plot
\

#### selectbox stock
# stock = st.selectbox('Choose a stock: ', 
#                        options = list_of_stocks)

# @st.cache
# def make_stocks():
#     stocks = go.Figure(
#             data=[go.Candlestick(x=data_stocks['date'][data_stocks['stocks']== stock],
#                                 open=data_stocks['open'],
#                                 high=data_stocks['high'],
#                                 low=data_stocks['low'],
#                                 close=data_stocks['close'],
#                                 increasing_line_color= 'rgb(37,130, 187)',
#                                 decreasing_line_color= 'rgb(213, 71, 115)')])
#     stocks.update_layout(#title_text='New cases reported in the last 7 days in {}'.format(country),
#                          font=dict(size=16),
#                          paper_bgcolor='whitesmoke',
#                          plot_bgcolor='rgba(0,0,0,0)',
#                          height=550)

#     return stocks

# stocks = make_stocks()
# stocks







