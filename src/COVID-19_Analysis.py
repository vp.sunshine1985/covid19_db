#!/usr/bin/env python
# coding: utf-8

# # COVID-19 : ANALYSIS

# In[255]:


# Importing Libararies for Data Analysis
import numpy as np
import pandas as pd
import warnings
warnings.filterwarnings("ignore")
# Importing Libararies for Visualization
import matplotlib.pyplot as plt
import seaborn as sns
get_ipython().run_line_magic('matplotlib', 'inline')

#set Jupyter max row display
pd.set_option('display.max_row',1000)

#set Jupyter max column width to 50
pd.set_option('display.max_columns',50)
#Setting up Current Working Directory
import os
os.getcwd()


# ### <font color=black> Load Data Set </font>

# In[256]:


def load_dataSet(URL):
    df = pd.read_csv(URL) 
    return df


# ### <font color=black> Data Set Inspection.. </font>

# In[257]:


def dataSetInspection(base_dataset):
       print("SHAPE :", base_dataset.shape)
       print("---------------------------")
       print("INFO :")
       print(base_dataset.info())
       print("---------------------------")
       print("COLUMNS :", base_dataset.columns)
       print("---------------------------")
       # List of Continuous Varables continuous
       print("CONTINUOUS VARIABLES/COLUMNS :", base_dataset.describe().columns)
       print("NUMBER OF CONTINUOUS COLUMNS:", len(base_dataset.describe().columns))
       # List of Categorical Variables
       print("CATEGORICAL VARIABLES/COLUMNS :", base_dataset.describe(include=object).columns)
       print("NUMBER OF CATEGORICAL COLUMNS:", len(base_dataset.describe(include=object).columns))
       print("---------------------------")
       print("BASIC STATISTICAL DETAILS :")
       print(base_dataset.describe())


# ### <font color=black> Duplicate Check.. </font>

# In[258]:


# Check duplicate records on 'State' column in each dataframe
def duplicate(x):
    df = x.duplicated(['State']).sum()
    print(df)

# Drop duplicate records from each dataframe
def drop_duplicate(x):
    x = x.drop_duplicates(subset = ['State'], keep=False, inplace=True)


# ### <font color=black> Missing Value/Null Value Treatment.. </font>

# In[259]:


def nullvalue_function(base_dataset,percentage):
    # Checking the Null Value Occurance
    print("SHAPE BEFORE NULL TREATMENT :", base_dataset.shape)
    print("COLUMN WISE NULL PERCENTAGE :")
    print(((base_dataset.isna().sum()/base_dataset.shape[0])*100).sort_values(0,ascending=False))
    
    # Converting into Null Value Percentage Table
    null_value_table=pd.DataFrame((base_dataset.isna().sum()/base_dataset.shape[0])*100).sort_values(0,ascending=False)
    null_value_table.columns=["Null Percentage"]
    
    #Defining Threshhold values 
    #null_value_table[null_value_table["Null Percentage"]>percentage].index
    
    #Drop the Columns that has null Values more than threshold
    base_dataset.drop(null_value_table[null_value_table["Null Percentage"]>percentage].index,axis=1,inplace=True)
    
    #Replace the null values ith Median() # Continuous Variables
    for i in base_dataset.describe().columns:
        base_dataset[i].fillna(base_dataset[i].median(),inplace=True)
    
    # Replace null values with Mode() # Categorical Variable
    for i in base_dataset.describe(include=object).columns:
        base_dataset[i].fillna(base_dataset[i].mode(),inplace=True)
    
    print("-----------------------------")
    print("NULL VALUE TREATED DATA SET: ")
    print("SHAPE :", base_dataset.shape)
    print("ANY NULL VALUES :", base_dataset.isnull().values.any())
    print("COUNT OF NULL VALUES :", base_dataset.isnull().values.sum())
    
    return base_dataset


# ### <font color=black> Outlier Treatment.. </font>

# In[260]:


# def outliers(df):
#     import statistics as sts
#     for i in df.describe().columns:
#         x=np.array(df[i])
#         p=[]
#         Q1 = df[i].quantile(0.25)
#         Q3 = df[i].quantile(0.75)
#         IQR = Q3 - Q1
#         LTV = Q1 - (1.5 * IQR)
#         UTV = Q3 + (1.5 * IQR)
#         for j in x:
#             if j <= LTV or j >= UTV:
#                 p.append(sts.median(x))
#             else:
#                 p.append(j)
#                 df[i]=p
#     return df       


# - ###### <font color=black> Loading Data Set For India - https://api.covid19india.org/csv/latest/ </font>

# In[261]:


#Importing Data Sets
#Fetch publically avaialable data from the Indian Government Site - INDIA & INDIAN STATES
COVID19_STATE_WISE_URL = 'https://api.covid19india.org/csv/latest/state_wise.csv'
COVID19_DATE_WISE_URL = 'https://api.covid19india.org/csv/latest/case_time_series.csv'
COVID19_STATE_WISE_DAILY_URL = 'https://api.covid19india.org/csv/latest/state_wise_daily.csv'
COVID19_DISTRICT_WISE_URL = 'https://api.covid19india.org/csv/latest/district_wise.csv'
COVID19_ICMR_DATA_URL = 'https://api.covid19india.org/csv/latest/tested_numbers_icmr_data.csv'
#COVID19_WORLDWIDE_DATA_URL = "https://www.ecdc.europa.eu/sites/default/files/documents/COVID-19-geographic-disbtribution-worldwide.xlsx"


# In[262]:


#Load All the Data Sets
COVID19_STATE_WISE_DS = load_dataSet(COVID19_STATE_WISE_URL)
COVID19_DATE_WISE_DS = load_dataSet(COVID19_DATE_WISE_URL)
COVID19_STATE_WISE_DAILY_DS = load_dataSet(COVID19_STATE_WISE_DAILY_URL)
COVID19_DISTRICT_WISE_DS = load_dataSet(COVID19_DISTRICT_WISE_URL)
COVID19_ICMR_DATA_DS = load_dataSet(COVID19_ICMR_DATA_URL)


# - ###### <font color=black> Inspecting Data Set - COVID19_STATE_WISE_DS(state_wise.csv)  </font>

# In[263]:


dataSetInspection(COVID19_STATE_WISE_DS)


# <font color=blue>======Inspection Insights========</font>
# - This data set contains more than 251 rows/records and 2 columns
# - out of 2 columns 1 column is continuous variable while 1 is categorical variables
# - And has data types as: int64(1), object(1)

# In[264]:


dataSetInspection(COVID19_DATE_WISE_DS)


# - ###### <font color=black> Duplicate Check Data Set - COVID19_STATE_WISE_DS(state_wise.csv)  </font>

# In[265]:


duplicate(COVID19_STATE_WISE_DS)


# In[266]:


drop_duplicate(COVID19_STATE_WISE_DS)
duplicate(COVID19_STATE_WISE_DS)


# - ###### <font color=black> Null Value Treatment to Data Set - COVID19_STATE_WISE_DS(state_wise.csv)  </font>

# In[267]:


df_COVID19_STATE_WISE_DS=nullvalue_function(COVID19_STATE_WISE_DS,70)


# In[268]:


df_COVID19_DATE_WISE_DS=nullvalue_function(COVID19_DATE_WISE_DS,70)


# In[269]:


df_COVID19_STATE_WISE_DAILY_DS=nullvalue_function(COVID19_STATE_WISE_DAILY_DS,70)


# In[270]:


df_COVID19_DISTRICT_WISE_DS=nullvalue_function(COVID19_DISTRICT_WISE_DS,70)


# In[271]:


df_COVID19_ICMR_DATA_DS=nullvalue_function(COVID19_ICMR_DATA_DS,70)


# - ###### <font color=black> Outlier Treatment to Data Set - COVID19_STATE_WISE_DS(state_wise.csv)  </font>

# In[272]:


# df_COVID19_STATE_WISE_DS=outliers(df_COVID19_STATE_WISE_DS)


# - ###### <font color=black>Addional Data Preparation Steps - COVID19_STATE_WISE_DS(state_wise.csv)  </font>

# In[273]:


#Update State to India where state code is TT and State as 'Total'
df_COVID19_STATE_WISE_DS.loc[df_COVID19_STATE_WISE_DS['State'] == 'Total', 'State'] = "India"
df_COVID19_STATE_WISE_DS.loc[df_COVID19_STATE_WISE_DS['State_code'] == 'TT', 'State_code'] = "IND"                          


# - ###### <font color=black> Final Analytical Data Sets - COVID19_STATE_WISE_DS(state_wise.csv)  </font>

# In[274]:


df_COVID19_STATE_WISE_DS.to_csv("state_wise_analyticalDataset.csv")
df_COVID19_DATE_WISE_DS.to_csv("case_time_series_analyticalDataset.csv")
df_COVID19_STATE_WISE_DAILY_DS.to_csv("state_wise_daily_analyticalDataset.csv")
df_COVID19_DISTRICT_WISE_DS.to_csv("district_wise_analyticalDataset.csv")
df_COVID19_ICMR_DATA_DS.to_csv("tested_numbers_icmr_data_analyticalDataset.csv")

print("DATA EXPORTED TO *analyticalDataset.CSV FILE SUCCESSFULLY!")


# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:




