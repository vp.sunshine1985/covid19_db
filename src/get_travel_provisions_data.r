base_url = "https://code.datasciencedojo.com/datasciencedojo/datasets/raw/master/Coronavirus/coronavirus_merged_sources_"

d = Sys.Date()-1


fname = paste0(substr(format(d,"%B"),0,3),day(d),year(d),".csv",sep='')


url = paste0(base_url,fname,sep='')

travel_provisions = read.csv(url)
travel_provisions = travel_provisions %>% select(-Cases,-New_cases,-Deaths,-New_deaths)

travel_provisions$script_runtime <- Sys.time()

filename = paste0("datasets/", "travel_provisions.csv")

write.csv(travel_provisions,filename,row.names = F,fileEncoding = 'UTF-8')

rm(list=ls())
